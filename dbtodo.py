from sqlalchemy import Integer, Date, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sqlalchemy import Column
from sqlalchemy.ext.declarative import declarative_base

import transaction_todo

Base = declarative_base()


class Task(Base):
    __tablename__ = 'tasks'
    ln = Column('ln',
                Integer,
                primary_key=True)
    date_task = Column('Date_task',
                       Date)
    desc = Column('Description',
                  String)

    def __init__(self, ln, date_task, desc):
        self.ln = ln
        self.date_task = date_task
        self.desc = desc


class DBTransaction(transaction_todo.Transaction):
    def __init__(self, host, user, password):
        super().__init__()
        database = 'todolist'
        self.engine = create_engine('mysql+mysqlconnector://' + user + ':' + password + '@' + host + '/' + database,
                                    echo=True)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        Base.metadata.create_all(self.engine)

    def read(self):
        result = self.session.query(Task).all()
        print('RESULT IS: ')
        read_tasks = []
        print(result)
        for row in result:
            task = {'LN': row.ln, 'Desc': row.desc, 'Date': row.date_task}
            read_tasks.append(task)
        print('READ tasks are: ')
        print(read_tasks)
        return read_tasks

    def write(self, tasks):
        print('writing tasks to table task in database todolist')
        for task in tasks:
            task_table = Task(task['LN'], task['Date'], task['Desc'])
            self.session.add(task_table)
            self.session.commit()
        self.session.commit()
        self.session.close()
