import csv
import shutil
import transaction_todo
from tempfile import NamedTemporaryFile


class FileTransaction(transaction_todo.Transaction):
    def __init__(self, filename):
        super().__init__()
        self.filename = filename

    def read(self):  # utility function to read csv file and return the list of tasks.
        fields = ['LN', 'Desc', 'Date']
        reslist = []
        try:
            with open(self.filename, 'r') as csvfile:
                reader = csv.DictReader(csvfile, fieldnames=fields)
                for row in reader:
                    reslist.append({'LN': row['LN'], 'Desc': row['Desc'], 'Date': row['Date']})
        except FileNotFoundError:
            with open(self.filename, 'w'):
                print('file does not exist creating file')
            with open(self.filename, 'r') as csvfile:
                reader = csv.DictReader(csvfile, fieldnames=fields)
                for row in reader:
                    reslist.append({'LN': row['LN'], 'Desc': row['Desc'], 'Date': row['Date']})
        return reslist

    def write(self, tasks):  # utility function to write the list of tasks.
        tempfile = NamedTemporaryFile(mode='w', delete=False)
        fields = ['LN', 'Desc', 'Date']
        with tempfile:
            writer = csv.DictWriter(tempfile, fieldnames=fields)
            for task in tasks:
                row = {'LN': task['LN'], 'Desc': task['Desc'], 'Date': task['Date']}
                writer.writerow(row)
        shutil.move(tempfile.name, self.filename)
