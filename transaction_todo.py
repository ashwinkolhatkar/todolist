import datetime
from contextlib import contextmanager


class Transaction:
    def __init__(self):
        self.tasks = []

    @contextmanager
    def display_result(self, function_name, tasks):
        print(function_name, ' called')
        print('Tasks before ', function_name)
        self.display_tasks(tasks)
        try:
            yield None
        finally:
            print('Tasks after ', function_name)
            self.display_tasks(tasks)
            print('----------------------------')

    def add_task(self, tasks, task):
        ln = len(tasks)
        row = {'LN': ln + 1, 'Desc': task, 'Date': datetime.date.today()}
        tasks.append(row)
        return tasks

    def update_task(self, tasks, task, line_no):
        if not tasks:
            print('no tasks to be updated.')
        else:
            row = tasks[line_no - 1]
            tasks[line_no - 1] = {'LN': row['LN'], 'Desc': task, 'Date': datetime.date.today()}
        return tasks

    def display_tasks(self, tasks):
        print('Line No\tTask\tDate')
        for task in tasks:
            print(str(task['LN']) + '\t' + str(task['Desc']) + '\t' + str(task['Date']))

    def remove_task(self, tasks, line_no):
        if not tasks:
            print('no tasks to remove')
        else:
            task = tasks[line_no - 1]
            tasks.remove(task)
        return tasks

    def read(self):
        raise Exception('not implemented')

    def write(self, tasks):
        raise Exception('not implemented')
