import sys
import dbtodo
import filetodo
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


def main():
    option = sys.argv[1]
    if option == 'new':
        file_transaction = filetodo.FileTransaction(os.getenv("CSV_FILE"))
        process(file_transaction, sys.argv[2], file_transaction.read())
        print('The tasks are: ')
        file_transaction.display_tasks(file_transaction.tasks)
    if option == '--db':
        db_transaction = dbtodo.DBTransaction(os.getenv("DB_HOSTNAME"), os.getenv("DB_USERNAME"),
                                              os.getenv("DB_PASSWORD"))  # 'localhost', 'root', 'ashwin'
        process(db_transaction, sys.argv[2], db_transaction.read())
        db_transaction.display_tasks(db_transaction.tasks)
    if option == 'exit':
        sys.exit(0)


def process(transaction, command_file, init_tasks):
    if command_file is not None:
        transaction.tasks = init_tasks
        print('initialised tasks list to:')
        transaction.display_tasks(transaction.tasks)
        with open(command_file, 'r') as f:
            lines = f.readlines()
            for line in lines:
                command = line.split(' ')
                option = command[0]
                if option == 'list':
                    transaction.display_tasks(transaction.tasks)
                    continue
                with transaction.display_result(option, transaction.tasks):
                    if option == 'add':
                        transaction.add_task(transaction.tasks, command[1])
                    if option == 'update':
                        transaction.update_task(transaction.tasks, command[2], int(command[1]))  # task, line_no
                    if option == 'remove':
                        transaction.remove_task(transaction.tasks, int(command[1]))  # remove line_no

        choice = input('commit changes? (y/n)')
        if choice == 'y':
            transaction.write(transaction.tasks)
            print('Changes committed')
            committed_tasks = transaction.read()
            transaction.display_tasks(committed_tasks)
        else:
            sys.exit(0)
    else:
        transaction.tasks = []


if __name__ == "__main__":
    main()
