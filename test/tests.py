import unittest
from parameterized import parameterized
import sys

sys.path.append('..')
import transaction_todo
from datetime import date


class MyTestCase(unittest.TestCase):

    @parameterized.expand([
        # testing on empty list
        ([], [{'LN': 1, 'Desc': "task 2", 'Date': date.today()}]),
        # testing on non-empty list
        ([{'LN': 1, 'Desc': "task 1", 'Date': date.today()}], [{'LN': 1, 'Desc': "task 1", 'Date': date.today()},
                                                               {'LN': 2, 'Desc': "task 2", 'Date': date.today()}]),
    ])
    def test_add(self, tasks, expected_tasks):
        transaction = transaction_todo.Transaction()
        tasks = transaction.add_task(tasks, "task 2")
        self.assertEqual(expected_tasks, tasks)

    @parameterized.expand([
        # non-empty tasks list
        ([{'LN': 1, 'Desc': "task 1", 'Date': date.today()},
          {'LN': 2, 'Desc': "task 2", 'Date': date.today()},
          {'LN': 3, 'Desc': "task 3", 'Date': date.today()}], [{'LN': 1, 'Desc': "task 1", 'Date': date.today()},
                                                               {'LN': 2, 'Desc': "task 4", 'Date': date.today()},
                                                               {'LN': 3, 'Desc': "task 3", 'Date': date.today()}]),
        # empty tasks list
        ([], [])
    ])
    def test_update(self, tasks, expected_tasks):
        transaction = transaction_todo.Transaction()
        tasks = transaction.update_task(tasks, "task 4", 2)
        self.assertEqual(expected_tasks, tasks)

    @parameterized.expand([
        # non-empty list of tasks
        ([{'LN': 1, 'Desc': "task 1", 'Date': date.today()},
          {'LN': 2, 'Desc': "task 2", 'Date': date.today()},
          {'LN': 3, 'Desc': "task 3", 'Date': date.today()}], [{'LN': 1, 'Desc': "task 1", 'Date': date.today()},
                                                               {'LN': 3, 'Desc': "task 3", 'Date': date.today()}]),
        # empty list of tasks
        ([], [])
    ])
    def test_remove(self, tasks, expected_tasks):
        transaction = transaction_todo.Transaction()
        tasks = transaction.remove_task(tasks, 2)
        self.assertEqual(expected_tasks, tasks)


if __name__ == '__main__':
    unittest.main()
